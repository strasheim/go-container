From golang:latest

RUN curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.61.0

ENV CGO_ENABLED=0
ENV GOOS=linux

RUN go version
